from PyQt4 import QtCore, QtGui, Qt
from server import Server
from shutil import copyfile
import os
import ast_parser
from threading import Thread, Event
import run_tab
from time import time
import datetime
import logging

try:
    _from_utf8 = QtCore.QString.fromUtf8
except AttributeError:

    def _from_utf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


# Main window gui
class UiMainWindow(QtGui.QWidget):
    showMessageBox = QtCore.pyqtSignal(str, str, str, str, str)

    def __init__(self, main_window):
        super(UiMainWindow, self).__init__()

        self.responses = {}
        self.showMessageBox.connect(self.on_show_message_box)

        self.cluster_files = []
        self.my_server = None
        self.num_threads_computers = {}
        self.num_threads_programs = {}
        self.programs_globals = {}

        main_window.setObjectName(_from_utf8("main_window"))
        main_window.resize(782, 494)
        main_window.setAutoFillBackground(False)
        main_window.setStyleSheet(_from_utf8("background-color: rgb(159, 209, 213);"))
        self.central_widget = QtGui.QWidget(main_window)
        self.central_widget.setObjectName(_from_utf8("central_widget"))
        self.gridLayout_2 = QtGui.QGridLayout(self.central_widget)
        self.gridLayout_2.setObjectName(_from_utf8("gridLayout_2"))
        self.tab_widget = QtGui.QTabWidget(self.central_widget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 209, 213))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        self.tab_widget.setPalette(palette)
        self.tab_widget.setStyleSheet(_from_utf8("background-color: rgb(159, 209, 213);"))
        self.tab_widget.setUsesScrollButtons(True)
        self.tab_widget.setDocumentMode(False)
        self.tab_widget.setTabsClosable(False)
        self.tab_widget.setMovable(False)
        self.tab_widget.setObjectName(_from_utf8("tab_widget"))
        self.main_tab = QtGui.QWidget()
        self.main_tab.setObjectName(_from_utf8("main_tab"))
        self.gridLayout = QtGui.QGridLayout(self.main_tab)
        self.gridLayout.setObjectName(_from_utf8("gridLayout"))
        self.grid_layout_3 = QtGui.QGridLayout()
        self.grid_layout_3.setObjectName(_from_utf8("grid_layout_3"))
        self.files_group_box = QtGui.QGroupBox(self.main_tab)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.files_group_box.setFont(font)
        self.files_group_box.setStyleSheet(_from_utf8(""))
        self.files_group_box.setObjectName(_from_utf8("files_group_box"))
        self.gridLayout_4 = QtGui.QGridLayout(self.files_group_box)
        self.gridLayout_4.setObjectName(_from_utf8("gridLayout_4"))
        self.files_grid = QtGui.QGridLayout()
        self.files_grid.setObjectName(_from_utf8("files_grid"))
        self.files_list = QtGui.QListWidget(self.files_group_box)
        self.files_list.setAcceptDrops(True)
        self.files_list.setStyleSheet(_from_utf8("background-color: rgb(238, 244, 250);"))
        self.files_list.setObjectName(_from_utf8("files_list"))
        self.files_grid.addWidget(self.files_list, 0, 0, 1, 3)
        self.add_file_btn = QtGui.QPushButton(self.files_group_box)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.add_file_btn.setFont(font)
        self.add_file_btn.setStyleSheet(_from_utf8("background-color: rgb(200, 234, 194);"))
        self.add_file_btn.setObjectName(_from_utf8("add_file_btn"))
        self.files_grid.addWidget(self.add_file_btn, 1, 0, 1, 1)
        self.remove_file_btn = QtGui.QPushButton(self.files_group_box)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.remove_file_btn.setFont(font)
        self.remove_file_btn.setStyleSheet(_from_utf8("background-color: rgb(200, 234, 194);"))
        self.remove_file_btn.setObjectName(_from_utf8("remove_file_btn"))
        self.files_grid.addWidget(self.remove_file_btn, 1, 1, 1, 1)
        self.run_btn = QtGui.QPushButton(self.files_group_box)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.run_btn.setFont(font)
        self.run_btn.setStyleSheet(_from_utf8("background-color: rgb(200, 234, 194);"))
        self.run_btn.setObjectName(_from_utf8("run_btn"))
        self.files_grid.addWidget(self.run_btn, 1, 2, 1, 1)
        self.gridLayout_4.addLayout(self.files_grid, 0, 0, 1, 1)
        self.grid_layout_3.addWidget(self.files_group_box, 0, 0, 1, 1)
        self.computers_group_box = QtGui.QGroupBox(self.main_tab)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.computers_group_box.setFont(font)
        self.computers_group_box.setObjectName(_from_utf8("computers_group_box"))
        self.gridLayout_5 = QtGui.QGridLayout(self.computers_group_box)
        self.gridLayout_5.setObjectName(_from_utf8("gridLayout_5"))
        self.computers_grid = QtGui.QGridLayout()
        self.computers_grid.setObjectName(_from_utf8("computers_grid"))
        self.add_myself_btn = QtGui.QPushButton(self.computers_group_box)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.add_myself_btn.setFont(font)
        self.add_myself_btn.setStyleSheet(_from_utf8("background-color: rgb(200, 234, 194);"))
        self.add_myself_btn.setObjectName(_from_utf8("add_myself_btn"))
        self.computers_grid.addWidget(self.add_myself_btn, 1, 0, 1, 1)
        self.remove_myself_btn = QtGui.QPushButton(self.computers_group_box)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.remove_myself_btn.setFont(font)
        self.remove_myself_btn.setStyleSheet(_from_utf8("background-color: rgb(200, 234, 194);"))
        self.remove_myself_btn.setObjectName(_from_utf8("remove_myself_btn"))
        self.computers_grid.addWidget(self.remove_myself_btn, 1, 1, 1, 1)
        self.computers_table = QtGui.QTableWidget(self.computers_group_box)
        self.computers_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        size_policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(self.computers_table.sizePolicy().hasHeightForWidth())
        self.computers_table.setSizePolicy(size_policy)
        self.computers_table.setStyleSheet(_from_utf8("background-color: rgb(238, 244, 250);"))
        self.computers_table.setShowGrid(True)
        self.computers_table.setColumnCount(3)
        self.computers_table.setObjectName(_from_utf8("computers_table"))
        self.computers_table.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.computers_table.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.computers_table.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.computers_table.setHorizontalHeaderItem(2, item)
        self.computers_table.horizontalHeader().setCascadingSectionResizes(False)
        self.computers_table.horizontalHeader().setDefaultSectionSize(100)
        self.computers_table.horizontalHeader().setMinimumSectionSize(30)
        self.computers_table.horizontalHeader().setStretchLastSection(True)
        self.computers_table.verticalHeader().setStretchLastSection(False)
        self.computers_grid.addWidget(self.computers_table, 0, 0, 1, 2)
        self.gridLayout_5.addLayout(self.computers_grid, 0, 0, 1, 1)
        self.grid_layout_3.addWidget(self.computers_group_box, 0, 1, 1, 1)
        self.gridLayout.addLayout(self.grid_layout_3, 0, 0, 1, 1)
        self.tab_widget.addTab(self.main_tab, _from_utf8(""))
        self.gridLayout_2.addWidget(self.tab_widget, 0, 0, 1, 1)
        main_window.setCentralWidget(self.central_widget)
        self.menu_bar = QtGui.QMenuBar(main_window)
        self.menu_bar.setGeometry(QtCore.QRect(0, 0, 782, 21))
        self.menu_bar.setObjectName(_from_utf8("menu_bar"))
        self.menu_help = QtGui.QMenu(self.menu_bar)
        self.menu_help.setObjectName(_from_utf8("menu_help"))
        main_window.setMenuBar(self.menu_bar)
        self.open_program_rules = QtGui.QAction(main_window)
        self.open_program_rules.setObjectName(_from_utf8("open_program_rules"))
        self.menu_help.addAction(self.open_program_rules)
        self.menu_bar.addAction(self.menu_help.menuAction())

        self.add_file_btn.setEnabled(False)
        self.remove_file_btn.setEnabled(False)
        self.run_btn.setEnabled(False)
        self.remove_myself_btn.setEnabled(False)

        self.connect(self.add_file_btn, Qt.SIGNAL('clicked()'), self.add_file_clicked)
        self.connect(self.remove_file_btn, Qt.SIGNAL('clicked()'), self.remove_file_clicked)
        self.connect(self.run_btn, Qt.SIGNAL('clicked()'), self.run_clicked)
        self.connect(self.add_myself_btn, Qt.SIGNAL('clicked()'), self.add_myself_clicked)
        self.connect(self.remove_myself_btn, Qt.SIGNAL('clicked()'), self.remove_myself_clicked)

        self.remove_myself_btn.setVisible(False)

        self.threads_timer = TimerClass(0.5, self.update_computers_table)
        self.threads_timer.setDaemon(True)
        self.threads_timer.start()

        self.retranslate_ui(main_window)
        self.tab_widget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslate_ui(self, main_window):
        main_window.setWindowTitle(_translate("main_window", "Computer Cluster", None))
        self.files_group_box.setTitle(_translate("main_window", "Files", None))
        self.add_file_btn.setText(_translate("main_window", "Add File", None))
        self.remove_file_btn.setText(_translate("main_window", "Remove File", None))
        self.run_btn.setText(_translate("main_window", "Run", None))
        self.computers_group_box.setTitle(_translate("main_window", "Computers", None))
        self.add_myself_btn.setText(_translate("main_window", "Add Myself", None))
        self.remove_myself_btn.setText(_translate("main_window", "Remove Myself", None))
        item = self.computers_table.horizontalHeaderItem(0)
        item.setText(_translate("main_window", "Ip", None))
        item = self.computers_table.horizontalHeaderItem(1)
        item.setText(_translate("main_window", "Port", None))
        item = self.computers_table.horizontalHeaderItem(2)
        item.setText(_translate("main_window", "Number of threads", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.main_tab), _translate("main_window", "Main", None))
        self.menu_help.setTitle(_translate("main_window", "Help", None))
        self.open_program_rules.setText(_translate("main_window", "Open program rules", None))

    # opens an message box, used to show an error message if it occurs.
    def on_show_message_box(self, thread_name, severity, title, text, informative_text):
        msg = QtGui.QMessageBox()
        msg.setIcon(getattr(QtGui.QMessageBox, str(severity)))
        msg.setWindowTitle(title)
        msg.setText(text)
        msg.setInformativeText(informative_text)
        msg.setStandardButtons(QtGui.QMessageBox.Close)
        self.responses[str(thread_name)] = msg.exec_()

    # adds a file to cluster files (if it's a python code then changes it) and sends the file to all other computers.
    def add_file_clicked(self):
        print 'start add_file_clicked'
        logger.info('add_file_clicked')
        file_path = QtGui.QFileDialog.getOpenFileName(QtGui.QFileDialog(), 'Open file', 'c:\\',
                                                      "All Files (*.*);;Text Files (*.txt);;PythonFiles (*.py)")

        if file_path and (file_path not in self.cluster_files):
            valid_file = True
            self.cluster_files += file_path
            file_name = file_path.split('/')[-1]
            self.files_list.addItem(file_name)
            if file_name.split('.')[-1] == 'py':
                code, error = ast_parser.parse(file_path)
                if code:
                    with open(file_name, 'w') as f:
                        f.write(code)
                else:
                    valid_file = False
                    logger.error(str(error))
            else:
                copyfile(file_path, file_name)
            if valid_file:
                for client in self.my_server.my_clients.clients.values():
                    client.send_binary_file(file_name)
        print 'end add_file_clicked'

    # remove a file from cluster files
    def remove_file_clicked(self):
        print 'start remove_file_clicked'
        logger.info('remove_file_clicked')
        item = self.files_list.currentItem()
        if item:
            file_name = str(item.text())
            self.files_list.takeItem(self.files_list.currentRow())
            if os.path.exists(file_name):
                os.remove(file_name)
        print 'end remove_file_clicked'

    # run the program
    def run_clicked(self):
        print 'start run_clicked'
        logger.info('run_btn_clicked')
        item = self.files_list.currentItem()
        if item:
            file_name, file_type = str(item.text()).split('.')

            if file_type == 'py':
                self.num_threads_programs[file_name] = 0
                self.programs_globals[file_name] = []
                new_tab = QtGui.QWidget()
                self.tab_widget.addTab(new_tab, file_name)
                index = self.tab_widget.indexOf(new_tab)
                new_run_tab = run_tab.UiRunTab(new_tab, self.tab_widget, index, self.num_threads_programs, file_name,
                                               self.programs_globals[file_name])
                new_run_tab.file_name.setText(file_name)
                new_run_tab.threads_num.setText('0')
                new_run_tab.connect(new_run_tab.close_tab_btn, Qt.SIGNAL('clicked()'),
                                    lambda: self.close_tab_clicked(index))

                new_run_tab.open_globals_btn.setVisible(False)

                self.tab_widget.setCurrentIndex(index)
                if self.my_server:
                    self.my_server.my_clients.send_to_everyone('import ::: ' + file_name)
                    self.my_server.my_threads[file_name + '.main'] = Thread(target=self.main_worker,
                                                                            args=(new_run_tab, file_name))
                    self.my_server.my_threads[file_name + '.main'].setDaemon(True)
                    self.my_server.my_threads[file_name + '.main'].start()
        print 'end run_clicked'

    def close_tab_clicked(self, index):
        self.tab_widget.removeTab(index)

    # thread that runs the main function and starts all the timers
    def main_worker(self, tab, file_name):
        start_time = time()
        self.my_server.global_tables[file_name] = tab.globals_table
        time_timer = TimerClass(0.01, self.update_time, args=(tab, start_time))
        program_threads_timer = TimerClass(0.5, tab.update_threads)
        time_timer.setDaemon(True)
        program_threads_timer.setDaemon(True)
        time_timer.start()
        program_threads_timer.start()
        self.my_server.worker(None, file_name, 'main', ())
        time_timer.stop()
        program_threads_timer.stop()
        self.update_time(tab, start_time)
        self.update_computers_table()
        tab.update_threads()
        tab.close_tab_btn.setEnabled(True)

    @staticmethod
    # updates the time passed from the start of the run.
    def update_time(tab, start_time):
        tab.time_from_start.setText(str(datetime.timedelta(seconds=(time() - start_time)))[:-3])

    # updates number of threads in the computer's table
    def update_computers_table(self):
        for i in range(self.computers_table.rowCount()):
            num = self.num_threads_computers[str(self.computers_table.item(i, 0).text())]
            self.computers_table.setItem(i, 2, QtGui.QTableWidgetItem(str(num)))

    # create the server and add the computer to the system
    def add_myself_clicked(self):
        print 'start add_myself_clicked'
        logger.info('add_myself_clicked')
        self.my_server = Server(self, self.files_list, self.computers_table,
                                self.num_threads_computers, self.num_threads_programs)
        row = self.computers_table.rowCount()
        self.computers_table.insertRow(row)
        address = self.my_server.address
        self.computers_table.setItem(row, 0, QtGui.QTableWidgetItem(address[0]))
        self.computers_table.setItem(row, 1, QtGui.QTableWidgetItem(str(address[1])))
        self.computers_table.setItem(row, 2, QtGui.QTableWidgetItem('0'))
        self.add_myself_btn.setEnabled(False)
        self.remove_myself_btn.setEnabled(True)
        self.add_file_btn.setEnabled(True)
        self.remove_file_btn.setEnabled(True)
        self.run_btn.setEnabled(True)
        print 'end add_myself_clicked'

    # remove the computer from the system
    def remove_myself_clicked(self):
        pass
        # self.my_server.my_clients.send_to_everyone('remove me ::: ' + str(self.my_server.address))
        # self.my_server.my_clients.clients = {}
        # self.close_event()
        # while self.computers_table.rowCount() > 0:
        #         self.computers_table.removeRow(0)
        # self.add_myself_btn.setEnabled(True)
        # self.remove_myself_btn.setEnabled(False)
        # self.add_file_btn.setEnabled(False)
        # self.remove_file_btn.setEnabled(False)
        # self.run_btn.setEnabled(False)

    # stop the server and the threads timer
    def close_event(self):
        if self.my_server:
            self.threads_timer.stop()
            self.my_server.stop_server = True
            self.my_server.server_socket.settimeout(0)
            # self.my_server.listener.settimeout(0)
            self.my_server.listener.close()


# allows to call a function every (interval) seconds
class TimerClass(Thread):
    def __init__(self, interval, target, args=()):
        Thread.__init__(self)
        self.event = Event()
        self.interval = interval
        self.target = target
        self.args = args

    def run(self):
        while not self.event.is_set():
            self.target(*self.args)
            self.event.wait(self.interval)

    def stop(self):
        self.event.set()


logger = logging.getLogger(__name__)
FORMAT = '%(asctime)-15s %(levelname)s:%(name)s  %(process)d %(thread)d %(name)s %(lineno)d   %(message)s'
logging.basicConfig(filename='app_log.log', format=FORMAT, level=logging.DEBUG)
log_handler = logging.StreamHandler()
log_handler.setFormatter(logging.Formatter(FORMAT))
logger.addHandler(log_handler)

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    window = QtGui.QMainWindow()
    ui = UiMainWindow(window)
    app.aboutToQuit.connect(ui.close_event)
    window.show()
    sys.exit(app.exec_())
