"""
Maria Benditkis
Cluster client
"""
import socket
import random
import logging
import threading
import os


# ######################################################################## AllClients
class AllClients(object):
    def __init__(self, bufsize):
        self.clients = {}
        self.BUFSIZE = bufsize

    def add_client(self, address):
        if not str(address) in self.clients.keys():
            self.clients[address[0]] = Client(address, self.BUFSIZE)

    def remove_client(self, address):
        self.clients.pop(address, None)

    def send_to_everyone(self, message):
        count = 0
        for client in self.clients.values():
            count += 1
            client.send_message(message)

    def send_to_random(self, message):
        address = random.choice(self.clients.keys())
        self.clients[address].send_message(message)


# ######################################################################## Client
class Client(object):
    def __init__(self, address, bufsize):
        self.address = address
        self.BUFSIZE = bufsize

    def send_message(self, message):
        logger.info('send message ')
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # print 'before connect'
        sock.connect(self.address)
        # print 'after connect'
        if len(message) > self.BUFSIZE:
            message = bytearray(message)
            sock.send('long message ::: ' + str(len(message)))
            logger.debug('sent long message notification')
            # print 'sent long message notification'
            ans = sock.recv(self.BUFSIZE)
            logger.debug('received ' + ans)
            # print 'received ' + ans
            while len(message) > self.BUFSIZE:
                sock.send(message[:self.BUFSIZE])
                message = message[self.BUFSIZE:]
        sock.send(message)
        logger.debug('sent all message')

        # logger.debug(' b r  message: ' + repr(message))
        # print 'before recv ' + message
        answer = sock.recv(self.BUFSIZE)
        logger.debug('received ' + answer)
        # logger.debug(' a r')
        if answer != 'ok':
            logger.warning('received %s!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!' % answer)
        sock.close()

    def send_binary_file(self, file_path):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(self.address)
        file_length = int(os.path.getsize(file_path))
        sock.send('binary file ::: ' + str(file_path) + ',,, ' + str(file_length))
        sock.recv(self.BUFSIZE)
        with open(file_path, 'rb') as bin_file:
            file_part = bin_file.read(self.BUFSIZE)
            while file_part:
                # logger.debug(' b s')
                sock.send(file_part)
                # logger.debug(' a s')
                file_part = bin_file.read(self.BUFSIZE)
        sock.close()


lock = threading.Lock()
logger = logging.getLogger(__name__)
