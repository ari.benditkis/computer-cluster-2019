"""
Quick sort testing program for computer milestone1
"""
from threading import Thread, Lock
import random
from time import time
import datetime


def qsort(left, right):
    global ls, threads_num
    logger.info('start qsort l r %d %d %s ~~~' % (left, right, ls[left:right+1]))
    my_lst = ls

    i = left
    j = right
    pivot = my_lst[(left + right)/2]
    while i <= j:
        while pivot > my_lst[i]:
            i = i+1
        while pivot < my_lst[j]:
            j = j-1
        if i <= j:
            my_lst[i], my_lst[j] = my_lst[j], my_lst[i]
            i = i + 1
            j = j - 1
    ls = my_lst

    l_thread = None
    r_thread = None

    logger.info('before new threads l j i r' + str(left) + ' ' + str(j) + ' ' + str(i) + ' ' + str(right) + '@@@')
    if left < j:
        logger.info('start thread l j %d %d %s' % (left, j, str(ls[left:j+1])))
        # if j-left < 5:
        #     qsort(left, j)
        # else:
        l_thread = Thread(target=qsort, args=(left, j))
        l_thread.start()
        logger.info('thread %d %d started' % (left, j))
        change_threads_num(1)
    if i < right:
        logger.info('start thread i r %d %d %s' % (i, right, str(ls[i:right+1])))
        # if right-i < 5:
        #     qsort(i, right)
        # else:
        r_thread = Thread(target=qsort, args=(i, right))
        r_thread.start()
        logger.info('thread %d %d started' % (i, right))
        change_threads_num(1)

    if l_thread is not None:
        logger.debug('thread %d %d before join' % (left, j))
        l_thread.join()
        logger.debug('thread %d %d after join' % (left, j))
        change_threads_num(-1)
    if r_thread is not None:
        logger.debug('thread %d %d before join' % (i, right))
        r_thread.join()
        logger.debug('thread %d %d after join' % (i, right))
        change_threads_num(-1)


def check_sorted():
    for i in range(len(ls)-1):
        if ls[i] > ls[i+1]:
            return False
    return True


def change_threads_num(add):
    global threads_num
    threads_num = threads_num + add


threads_num = 0
start_time = time()
ls = random.sample(xrange(300), 300)
# ls = [19, 10, 7, 16, 13, 17, 11, 15, 12, 0, 14, 18, 2, 5, 8, 9, 3, 4, 6, 1]
logger.info(ls)
logger.debug('before sort')
qsort(0, len(ls) - 1)
logger.debug('after sort')
ls_sorted = check_sorted()
if ls_sorted:
    logger.info('SORTED!!!!!!!!!!!!!!!!!!!!!!!!!')
else:
    logger.warning('NOT SORTED?!?!?!?!?!?!?!?!?!?!?!')
logger.info(ls)
logger.info(str(datetime.timedelta(seconds=(time() - start_time)))[:-3])
